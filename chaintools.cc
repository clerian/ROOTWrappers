#include "chaintools.hh"

TChain *openchain(std::string chainname,std::vector<std::string> filename){
  TChain *chain = new TChain(chainname.c_str());
  addfiletochain(chain,filename);
  return chain;  
}

bool addfiletochain(TChain *chain, std::vector<std::string> filename){
  if(chain == NULL) return false;
  for(auto i=filename.begin(); i<filename.end(); i++){
    chain->Add((*i).c_str());
  }  
  return true;
}

std::vector<std::array<size_t, 2> > getfileboundry(TChain *chain){
  std::vector<std::array<size_t, 2> > output;
  int totalentries = chain->GetEntries();
  
  int old = 0;
  for(int i=0; i<chain->GetNtrees(); i++){ // loop over TTrees start
    chain->GetEntry(old);
    TTree *tree = chain->GetTree();
    int n = old+tree->GetEntries();
    // divide it into junks of 2000 entries
    int diff = n-old;
    while(diff>2000){
      std::array<size_t, 2> tmp = {{old, old+2000}};
      output.push_back(tmp);
      old += 2000;
      diff = n-old;
    }
      std::array<size_t, 2> tmp = {{old, n}};
      output.push_back(tmp);

    if(n<totalentries) old = n;
    tree = NULL;
  }// loop over TTrees start

  return output;
}
