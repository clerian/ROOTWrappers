# ROOTWrappers

this small library contains wrappers for the CERN ROOT histogram classes TH1, TH2 and TH3. The wrappers can be used with openmp to quickly get a pararell for loop with ROOT histograms

usage:
```C++
#include<OMPwrappers.hh>
#include<chaintools.hh>

#include<TH1.h>
#include<TBranch.h>
#include<TChain.h>
#include<vector>
#include<string>

int main(int argc, char *argv[])
  // At first we prepare a sample TChain
  std::vector<std::string> flist; // file list to open
  std::vector<TBranch **>  branches (numberOfBranches, NULL); 
  std::vector<double>      data (numberOfBranches);
  std::vector<std::string> branchnames (numberOfBranches);
  branchnames[0] = "testdata";

  flist.push_back("data.root");
  TChain* chain = openchain("T", flist); // open a TChain with TTree name 'T' containing all files in flist
  addtochain<double>(chain, &branches, branchnames, &data);

  OMPTH1<TH1I> h0("hist1", "A histogram" , 10,  0, 5); // create a histogram with 10 bins
  auto boundry = getfileboundry(chain);
  for (auto tree=boundry.begin(); tree<boundry.end(); tree++){
#pragma omp parallel for 
    for(int i=(*tree)[0]; i<(*tree)[1]; i++){ // readout loop start                                             
#pragma omp critical
      {
        chain->GetEntry(i);                                                        
      }
      h0->Fill(data[0]);
} // readout loop end
TH1D *h = h0.getgisto();
```