#include"OMPwrappers.hh"

OMPTH::OMPTH(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup){
  hname  = std::string(name);
  htitle = std::string(title);
  binsx = nbinsx;
  xl = xlow;
  xu = xup;
  maxthreads = omp_get_max_threads();
  data1 = new std::list<double>[maxthreads];
  data2 = NULL;
  data3 = NULL;
}

OMPTH::OMPTH(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup,
		Int_t nbinsy, Double_t ylow, Double_t yup) {
  hname  = std::string(name);
  htitle = std::string(title);
  binsx = nbinsx;
  xl = xlow;
  xu = xup;
  binsy = nbinsy;
  yl = ylow;
  yu = yup;
  maxthreads = omp_get_max_threads();
  data1 = new std::list<double>[maxthreads];
  data2 = new std::list<double>[maxthreads];
  data3 = NULL;
}

OMPTH::OMPTH(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup,
		Int_t nbinsy, Double_t ylow, Double_t yup, Int_t nbinsz, Double_t zlow, Double_t zup){
  hname  = std::string(name);
  htitle = std::string(title);
  binsx = nbinsx;
  xl = xlow;
  xu = xup;
  binsy = nbinsy;
  yl = ylow;
  yu = yup;
  binsz = nbinsz;
  zl = zlow;
  zu = zup;
  maxthreads = omp_get_max_threads();
  data1 = new std::list<double>[maxthreads];
  data2 = new std::list<double>[maxthreads];
  data3 = new std::list<double>[maxthreads];
}


void OMPTH::Fill(double val){
  if(!omp_in_parallel()){
    data1[0].push_back(val);
  } else {
    int i = omp_get_thread_num(); 
    data1[i].push_back(val);
  }
}

void OMPTH::Fill(double val, double val2){
  if(!omp_in_parallel()){
    data1[0].push_back(val);
    data2[0].push_back(val);
  } else {
    int i = omp_get_thread_num(); 
    data1[i].push_back(val);
    data2[i].push_back(val);
  }
}

void OMPTH::Fill(double val, double val2, double val3){
  if(!omp_in_parallel()){
    data1[0].push_back(val);
    data2[0].push_back(val2);
    data3[0].push_back(val3);
  } else {
    int i = omp_get_thread_num(); 
    data1[i].push_back(val);
    data2[i].push_back(val2);
    data3[i].push_back(val3);
  }
}


OMPTH::~OMPTH(){
  if(data1 != NULL) delete [] data1; 
  if(data2 != NULL) delete [] data2; 
  if(data3 != NULL) delete [] data3; 
}

