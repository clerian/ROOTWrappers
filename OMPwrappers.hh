#ifndef __OMPWARPPERS_HH__
#define __OMPWARPPERS_HH__
#include<omp.h>
#include<string>
#include<list>
#include<iostream>
#include<TH1.h>
#include<TH2.h>
#include<TH3.h>

//! OMP Wrapper base class for ROOT histograms
/*!
  This class defines three constructors that use the same sysntax as the 
  ROOT TH histograms. The main point is that the constructors allocate
  as many lists as OMP can produce threads. This way a non blocking
  filling of this buffers with the Fill() functions is possible.
 */
class OMPTH{
public:
  //! Constructor for TH1 histograms
  /*!
    @param name ROOT name of the histogram
    @param title histogram title
    @param nbinsx number of bins in X direction
    @param xlow lower boundry on X axis
    @param xup higher boundry on X axis
   */
  OMPTH(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup);
  //! Constructor for TH2 histograms
  /*!
    @param name ROOT name of the histogram
    @param title histogram title
    @param nbinsx number of bins in X direction
    @param xlow lower boundry on X axis
    @param xup higher boundry on X axis
    @param nbinsy number of bins in Y direction
    @param ylow lower boundry on Y axis
    @param yup higher boundry on Y axis
   */
  OMPTH(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup,
	Int_t nbinsy, Double_t ylow, Double_t yup);
  //! Constructor for TH3 histogrms
  /*!
    @param name ROOT name of the histogram
    @param title histogram title
    @param nbinsx number of bins in X direction
    @param xlow lower boundry on X axis
    @param xup higher boundry on X axis
    @param nbinsy number of bins in Y direction
    @param ylow lower boundry on Y axis
    @param yup higher boundry on Y axis
    @param nbinsz number of bins in Z direction
    @param zlow lower boundry on Z axis
    @param zup higher boundry on Z axis
   */
  OMPTH(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup,
	Int_t nbinsy, Double_t ylow, Double_t yup, Int_t nbinsz, Double_t zlow, Double_t zup);

  //! Fill function for TH1
  /*!
    @param val value to fill
   */
  void Fill(double val);
  //! Fill function for TH2
  /*!
    @param val value to fill in X direction
    @param val2 value to fill in Y direction
   */
  void Fill(double val, double val2);
  //! Fill function for TH3
  /*!
    @param val value to fill in X direction
    @param val2 value to fill in Y direction
    @param val3 value to fill in Z direction
   */
  void Fill(double val, double val2, double val3);
  
  //! sets label of X axis
  void SetXAxisLabel(std::string xaxis){xlabel = xaxis;}
  //! sets label of Y axis
  void SetYAxisLabel(std::string yaxis){ylabel = yaxis;}
  //! sets label of Z axis
  void SetZAxisLabel(std::string zaxis){zlabel = zaxis;}

  //! destructor
  ~OMPTH();

protected:
  std::string hname;            //!< name of the histogram	  
  std::string htitle;           //!< histogram title		  
  std::string xlabel;           //!< label of the X axis
  std::string ylabel;           //!< label of the Y axis
  std::string zlabel;           //!< label of the Z axis
  Int_t binsx;                  //!< number of bins in X direction 
  Double_t xl;                  //!< lower boundry on X axis	   
  Double_t xu;                  //!< higher boundry on X axis      

  Int_t binsy;                  //!< number of bins in Y direction         
  Double_t yl;                  //!< lower boundry on Y axis	   
  Double_t yu;                  //!< higher boundry on Y axis      

  Int_t binsz;                  //!< number of bins in Z direction 
  Double_t zl;                  //!< lower boundry on Z axis	   
  Double_t zu;                  //!< higher boundry on Z axis      
  
  int maxthreads;               //!< maximum number of threads

  std::list<double> *data1;     //!< data storage for TH1
  std::list<double> *data2;     //!< data storage for TH2
  std::list<double> *data3;     //!< data storage for TH3
};

//! OMP wrapper class for TH1 histograms
template <class T>
class OMPTH1 : public OMPTH {
public:
  //! constructor for TH1
  /*!
    @param name ROOT name of the histogram
    @param title histogram title
    @param nbinsx number of bins in X direction
    @param xlow lower boundry on X axis
    @param xup higher boundry on X axis
   */
  OMPTH1(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup):
    OMPTH(name, title, nbinsx, xlow, xup) {}

  //! returns histogram of specified type
  /*!
    this function allocates a new TH1 histogram and fills it with the values
    drom the @ref OMPTH::data1 lists.
   */
  T* gethisto();

  //! destructor
  ~OMPTH1(){}
};

//! OMP wrapper class for TH2 histograms
template <class T>
class OMPTH2 : public OMPTH {
public:
  //! constructor for TH2
  /*!
    @param name ROOT name of the histogram
    @param title histogram title
    @param nbinsx number of bins in X direction
    @param xlow lower boundry on X axis
    @param xup higher boundry on X axis
    @param nbinsy number of bins in Y direction
    @param ylow lower boundry on Y axis
    @param yup higher boundry on Y axis
   */
  OMPTH2(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup,
	Int_t nbinsy, Double_t ylow, Double_t yup):
    OMPTH(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup ) {}

  //! returns histogram of specified type
  /*!
    this function allocates a new TH2 histogram and fills it with the values
    drom the @ref OMPTH::data1 and @ref OMPTH::data2  lists.
   */
  T* gethisto();

  //! destructor
  ~OMPTH2(){}
};

//! OMP wrapper class for TH3 histograms
template <class T>
class OMPTH3 : public OMPTH {
public:
  //! constructor for TH3
  /*!
    @param name ROOT name of the histogram
    @param title histogram title
    @param nbinsx number of bins in X direction
    @param xlow lower boundry on X axis
    @param xup higher boundry on X axis
    @param nbinsy number of bins in Y direction
    @param ylow lower boundry on Y axis
    @param yup higher boundry on Y axis
    @param nbinsz number of bins in Z direction
    @param zlow lower boundry on Z axis
    @param zup higher boundry on Z axis
   */
  OMPTH3(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup,
	Int_t nbinsy, Double_t ylow, Double_t yup, Int_t nbinsz, Double_t zlow, Double_t zup):
    OMPTH(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup, nbinsz, zlow, zup) {}

  //! returns histogram of specified type
  /*!
    this function allocates a new TH3 histogram and fills it with the values
    drom the @ref OMPTH::data1 ,@ref OMPTH::data2 and @ref OMPTH::data3  lists.
   */
  T* gethisto();

  //! destructor
  ~OMPTH3(){}
};

#include"OMPwrappers.icc"

#endif
